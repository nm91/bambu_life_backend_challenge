function ageScoreCalculator(currentUserAge, matchingAge) {
    let ageDifference = currentUserAge - matchingAge;
    let score;
    const AGE_THRESHOLD = 10;
    if (ageDifference < 0) ageDifference = ageDifference * -1;
    if (ageDifference >= AGE_THRESHOLD || ageDifference == 0) {
        return 0;
    }
    switch(ageDifference) {
        case 1: 
            score = 0.9
            break;
        case 2:
            score = 0.8
            break;
        case 3: 
            score =  0.7;
            break;
        case 4:
            score = 0.6;
            break;
        case 5:
            score = 0.5;
            break;
        case 6:
            score = 0.4;
            break;
        case 7:
            score = 0.3;
            break;
        case 8:
            score = 0.2;
            break;
        case 9:
            score = 0.1;
            break;
    }
    return score;
}
function salaryScoreCalculator(currentUserSalary, matchingSalary) {
    let salaryDifference = currentUserSalary - matchingSalary;
    if (salaryDifference < 0) salaryDifference = salaryDifference * -1;
    const Salary_THRESHOLD = 9001;
    if (salaryDifference >= Salary_THRESHOLD)
        return 0;
    else if (salaryDifference == 0)
        return 1.0;
    else if (salaryDifference >= 500 && salaryDifference <=1000)
        return 0.9;
    else if (salaryDifference >= 1000 && salaryDifference <=2000)
        return 0.8;
    else if (salaryDifference >= 2000 && salaryDifference <=3000)
        return 0.7;
    else if (salaryDifference >= 3000 && salaryDifference <= 4000)
        return 0.6;
    else if (salaryDifference >= 4000 && salaryDifference <=5000)
        return 0.5;
    else if (salaryDifference >= 5000 && salaryDifference<=6000)
        return 0.4;
    else if (salaryDifference >= 6000 && salaryDifference <=7000)
        return 0.3;
    else if (salaryDifference >= 7000 && salaryDifference<=8000)
        return 0.2;
    else if (salaryDifference < 0 || (salaryDifference >= 8000 && salaryDifference <= 9000))
        return 0.1;
    else return 0;
}

function distanceScoreCalculator(distance) {
    const DISTANCE_THRESHOLD = 101
    if (distance > DISTANCE_THRESHOLD)
        return 0;
    else if (distance < 10)
      return 1.0
    else if (distance > 10 && distance < 20)
      return 0.9
    else if (distance > 20 && distance < 30)
      return 0.8
    else if (distance > 30  && distance < 40)
      return 0.7
    else if (distance > 40 && distance < 50)
      return 0.6
    else if (distance > 50 && distance < 60)
        return 0.5
    else if (distance > 60 && distance < 70)
      return 0.4
    else if (distance > 70 && distance < 80)
      return 0.3
    else if (distance > 80 && distance < 90)
      return 0.2
    else if (distance > 90 && distance < 100)
      return 0.1;
    else return 0.0;
}

function experienceScoreCalculator(currentUserExperience, matchingExperience) {
    let score = 0
    if (currentUserExperience == matchingExperience)
        score = 1;
    return score;
}

module.exports = {
    salaryScoreCalculator: salaryScoreCalculator,
    ageScoreCalculator: ageScoreCalculator,
    experienceScoreCalculator: experienceScoreCalculator,
    distanceScoreCalculator: distanceScoreCalculator
};