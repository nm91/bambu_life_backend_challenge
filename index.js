const express = require('express');
const app = express();
const morgan = require('morgan')
const _ = require('lodash');
const csv = require("fast-csv");
const promise = require('bluebird');
app.use(morgan('combined'))
const port = process.env.PORT || 3000;
const ruleEngine = require('./rules');
const utils = require('./utils');



app.get('/people-like-you', (req, res) => {
    let scoredDataObjects =  [];
    let startTime = new Date();
    let salaryScore, distanceScore, ageScore, expScore, scores = [];

    if (!req.query.age && !req.query.monthlyIncome && !req.query.experienced
    && !req.query.longitude && !req.query.latitude) {
        return res.status(200).json({
            'peopleLikeYou': scoredDataObjects
        });
    }
    csv.fromPath("./data/data.csv", {headers: true})
    .on("data", function(data){
        scores = [];
        if (req.query.age) {
            ageScore = ruleEngine.ageScoreCalculator(data.age, req.query.age);
            scores.push(ageScore);
        }
        if (req.query.monthlyIncome) {
            salaryScore = ruleEngine.salaryScoreCalculator(data['monthly income'], req.query.monthlyIncome);
            scores.push(salaryScore);
        }
        if (req.query.longitude && req.query.latitude) {
            distanceScore = utils.changeCoordinatesToKm(data, req.query.longitude, req.query.latitude);
            scores.push(distanceScore);
        }
        if (req.query.experienced) {
            expScore = ruleEngine.experienceScoreCalculator(data.experienced, req.query.experienced);
            scores.push(expScore);
        }
        if(scores.length >0) {
            let sum = 0.0;
            return promise.each(scores, (score) => {
                sum = (sum * 1) + (score * 1);
                sum = sum.toFixed(1)
            }).then(() => {
                if (sum <= 0) {
                    data.score = 0
                } else {
                    sum = sum * 1;
                    data.score =(sum / scores.length).toFixed(1);
                    sum = 0.0;
                    scoredDataObjects.push(data);
                }
            });
        }
    }).on("end", function(){
        scoredDataObjects = _.sortBy(scoredDataObjects, "score").reverse();
        scoredDataObjects = scoredDataObjects.slice(0,9);
        let endTime = new Date();
        let seconds = (endTime.getTime() - startTime.getTime()) / 1000;
        return res.status(200).json({
           'peopleLikeYou': scoredDataObjects,
           'resultsCalculatedIn': seconds
        });
    });
});

app.listen(port, function() {
    console.log('Our app is running on ' + port);
});