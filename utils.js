const ruleEngine = require('./rules');
const geolib = require('geolib');
function changeCoordinatesToKm(userData, latitude, longitude) {
    let miles = geolib.getDistance({
        latitude: latitude,
        longitude: longitude 
    }, {
        latitude: userData.latitude,
        longitude: userData.longitude
    });
    miles = geolib.convertUnit('km', miles, 2);
    return ruleEngine.distanceScoreCalculator(miles);
}

module.exports = {
    changeCoordinatesToKm: changeCoordinatesToKm
};